package com.chen.singleton;
/**
 * 单例模式
 *     懒加载，synchronized锁内置，避免并发性能问题 
 *     
 * 2015-8-28 20:47:12
 * @author jinlong.chen
 */
public class Singleton3 {
	private Singleton3() {
		System.out.println(" singleton3 constructor ~~ "+this.getClass().getName());
	}
	
	private static Singleton3 singleton = null;
	public static final Singleton3 getInstance() {
		if(singleton==null) {
			synchronized(Singleton3.class) {
				if(singleton==null) {
					singleton = new Singleton3();
				}
			} 
		}
		
		return singleton;
	}
	
	
	
	public void sayHello() {
		System.out.println("hello world ! ,  hashCode : "+this.hashCode());
	}
}