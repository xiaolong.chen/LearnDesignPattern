package com.chen.singleton;
/**
 * 单例模式
 *     非懒加载，不存在并发性能问题，初始化时间较长
 *     
 * 2015-8-28 20:47:12
 * @author jinlong.chen
 */
public class Singleton1 {
	private Singleton1() {
		System.out.println(" singleton1 constructor ~~ "+this.getClass().getName());
	}
	
	private static Singleton1 singleton = new Singleton1();
	public static final Singleton1 getInstance() {
		return singleton;
	}
	
	
	
	public void sayHello() {
		System.out.println("hello world ! ,  hashCode : "+this.hashCode());
	}
}