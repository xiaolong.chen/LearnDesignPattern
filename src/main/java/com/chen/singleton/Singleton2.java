package com.chen.singleton;
/**
 * 单例模式
 *     懒加载，synchronized加载getInstance方法上，存在并发性能问题
 *     
 * 2015-8-28 20:47:12
 * @author jinlong.chen
 */
public class Singleton2 {
	private Singleton2() {
		System.out.println(" singleton2 constructor ~~ "+this.getClass().getName());
	}
	
	private static Singleton2 singleton = null;
	public static final synchronized Singleton2 getInstance() {
		if(singleton==null) {
			singleton = new Singleton2();
		}
		return singleton;
	}
	
	
	
	public void sayHello() {
		System.out.println("hello world ! ,  hashCode : "+this.hashCode());
	}
}