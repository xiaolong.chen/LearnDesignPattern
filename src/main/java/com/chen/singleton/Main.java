package com.chen.singleton;
/**
 * 单例模式
 *    保证class仅有一个实例，并提供一个访问它的全局访问点
 * 2015-8-28 20:47:12
 * @author jinlong.chen
 */
public class Main {
	public static void main(String[] args) {
		
		Singleton1.getInstance().sayHello();
		Singleton1.getInstance().sayHello();
		Singleton1.getInstance().sayHello();
		
		
		Singleton2.getInstance().sayHello();
		Singleton2.getInstance().sayHello();
		Singleton2.getInstance().sayHello();
		
		
		Singleton3.getInstance().sayHello();
		Singleton3.getInstance().sayHello();
		Singleton3.getInstance().sayHello();
	}
}