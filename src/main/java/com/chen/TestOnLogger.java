package com.chen;

import org.junit.Test;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
/**
 * 测试Logback
 * 2016-11-29 16:09:02
 * @author chenjinlong
 */
public class TestOnLogger {
	
	
	@Test
	public void test02() {
		org.slf4j.Logger logger = LoggerFactory.getLogger("test");
		
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		loggerContext.start();
		
		ch.qos.logback.classic.Logger newLogger = (ch.qos.logback.classic.Logger) logger;
        newLogger.detachAndStopAllAppenders();

        //define appender
        RollingFileAppender<ILoggingEvent> rollingFileAppender = new RollingFileAppender<ILoggingEvent>();

        //policy
        TimeBasedRollingPolicy<ILoggingEvent> timeBasedRollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
        timeBasedRollingPolicy.setContext(loggerContext);
        timeBasedRollingPolicy.setMaxHistory(30);
        timeBasedRollingPolicy.setFileNamePattern("D:/log/test.%d{yyyy-MM-dd}.log");
        timeBasedRollingPolicy.setParent(rollingFileAppender);
        timeBasedRollingPolicy.start();

        //encoder
        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(loggerContext);
        //encoder.setPattern("%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n");
        encoder.setPattern("%msg%n");
        encoder.start();

        //start appender
        rollingFileAppender.setFile("D:/log/test.log");
        rollingFileAppender.setRollingPolicy(timeBasedRollingPolicy);
        rollingFileAppender.setContext(loggerContext);
        rollingFileAppender.setEncoder(encoder);
        rollingFileAppender.start();

        newLogger.addAppender(rollingFileAppender);
        newLogger.setLevel(Level.INFO);
        newLogger.setAdditive(false);
        
        newLogger.error("your json");
	}
}