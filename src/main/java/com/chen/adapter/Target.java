package com.chen.adapter;
/**
 * Target 目标角色 接口
 * 2016-4-20 16:30:08
 * @author jinlong.chen
 */
public interface Target {
	
	/**
	 * 既定的接口
	 * @param message
	 */
	public void request(String message);
}