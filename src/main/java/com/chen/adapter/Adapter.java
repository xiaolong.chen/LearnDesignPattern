package com.chen.adapter;

import lombok.extern.slf4j.Slf4j;
/**
 * 转换器角色
 * 2016-4-20 16:36:33
 * @author jinlong.chen
 */
@Slf4j
public class Adapter implements Target {
	private SourceObject sourceObject;
	
	public Adapter() {
		
	}
	public Adapter(SourceObject sourceObject) {
		this.sourceObject = sourceObject;
	}

	@Override
	public void request(String message) {
		log.info("Adapter do - ");
		sourceObject.doSth(message);
	}
}