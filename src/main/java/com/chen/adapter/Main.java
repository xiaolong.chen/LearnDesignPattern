package com.chen.adapter;
/**
 * 适配器模式：
 * 		将一个类的接口变换成客户端期待的另一种接口，从而使原本因接口不匹配无法在一起的两个类能够在一起工作
 * 		wapper包装
 * 
 * 		适配器主要场景是扩展应用中，系统扩展不符合原有设计时，考虑使用适配器模式减少代码修改带来的风险
 * 
 * 2016-4-20 16:00:43
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) {
		Target target = new ConcreteTarget();
		target.request("O(∩_∩)O哈哈哈~");
		
		//适配器，适配原有接口
		SourceObject sourceObject = new SourceObject();
		Target adapter = new Adapter(sourceObject);
		adapter.request("Ｏ(≧口≦O");
	}
}