package com.chen.adapter;

import lombok.extern.slf4j.Slf4j;
/**
 * Target 目标角色具体实现
 * 2016-4-20 16:30:08
 * @author jinlong.chen
 */
@Slf4j
public class ConcreteTarget implements Target {
	
	
	@Override
	public void request(String message) {
		log.info("ConcreteTarget request : {}",message);
	}
}