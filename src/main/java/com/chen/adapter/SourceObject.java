package com.chen.adapter;

import lombok.extern.slf4j.Slf4j;
/**
 * 需要被转换，适应现在接口的源角色
 * 2016-4-20 16:34:54
 * @author jinlong.chen
 */
@Slf4j
public class SourceObject {
	
	
	public void doSth(String message) {
		log.info("SourceObject doSth:{} ",message);
	}
}