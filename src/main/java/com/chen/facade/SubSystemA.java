package com.chen.facade;
/**
 * 子系统
 * 2015-08-25 21:58:07
 * @author aimin.chen
 */
public class SubSystemA {
	
	public void doSthA() {
		/**
		 * doSth 
		 */
		System.out.println("doSthA~~  "+this.getClass().getName());
	}
}