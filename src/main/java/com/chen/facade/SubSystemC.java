package com.chen.facade;
/**
 * 子系统
 * 2015-08-25 21:58:07
 * @author aimin.chen
 */
public class SubSystemC {
	
	public void doSthC() {
		/**
		 * doSth 
		 */
		System.out.println("doSthC ~~  "+this.getClass().getName());
	}
}