package com.chen.facade;
/**
 * 外观模式：
 * 		子系统的外部和内部通信通过一个统一对象进行，提供一个高层次的接口，使得子系统更易于使用
 * 		发生变更时，只改变内部处理逻辑，不改变对外暴露的接口方法即高层次接口不变
 * 
 * 	优点：减少相互依赖，提供灵活性，提高安全性
 *     注意：门面不参与业务，门面是稳定的，少变化
 * 2015-08-25 21:29:06
 * @author aimin.chen
 */
public class Main {
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		Facade facade = new Facade();
		
		facade.methodA();
		facade.methodB();
		facade.methodC();
		
		facade.methodD();
	}
}