package com.chen.facade;
/**
 * 门面类
 * 2015-08-25 21:58:07
 * @author aimin.chen
 */
public class Facade {
	private SubSystemA subSystemA;
	private SubSystemB subSystemB;
	private SubSystemC subSystemC;
	
	public Facade() {
		this.subSystemA = new SubSystemA();
		this.subSystemB = new SubSystemB();
		this.subSystemC = new SubSystemC();
	}
	
	public void methodA() {
		System.out.println(" methodA~~~~ ");
		subSystemA.doSthA();
		System.out.println("   ");
	}
	public void methodB() {
		System.out.println(" methodB~~~~ ");
		subSystemB.doSthB();
		System.out.println("   ");
	}
	public void methodC() {
		System.out.println(" methodC~~~~");
		subSystemC.doSthC();
		System.out.println("   ");
	}
	
	/**
	 * 门面类只对外提供了访问子系统的一个路径，不参与内部子系统的业务
	 * 否则会产生倒依赖的问题，子系统必须依赖门面才可以访问；破坏了系统的封装性
	 */
	@Deprecated
	public void methodD() {
		System.out.println("   @Deprecated function  ");
		subSystemA.doSthA();
		subSystemB.doSthB();
	}
}