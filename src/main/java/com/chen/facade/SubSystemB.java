package com.chen.facade;
/**
 * 子系统
 * 2015-08-25 21:58:07
 * @author aimin.chen
 */
public class SubSystemB {
	
	public void doSthB() {
		/**
		 * doSth 
		 */
		System.out.println("doSthB~~  "+this.getClass().getName());
	}
}