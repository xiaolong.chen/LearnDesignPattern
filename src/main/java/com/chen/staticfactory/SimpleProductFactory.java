package com.chen.staticfactory;

import com.chen.factorymethod.Product;
/**
 * 产品简单工厂模式提供的工厂类
 * 2015-8-26 11:21:53
 * @author jinlong.chen
 */
public class SimpleProductFactory {
	
	private SimpleProductFactory(){
		
	}
	
	
	/**
	 * 用反射 或者 if/switch 逻辑控制
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static final <T extends Product> T create(Class<T> clazz) {
		Product product = null;
		try {
			/**
			 * 调用默认参数空的构造方法 , newInstance()
			 */
			product = (Product) Class.forName(clazz.getName()).newInstance();
		}
		catch(Exception e) {
			
		}
		return (T) product;
	}
}