package com.chen.staticfactory;

import com.chen.factorymethod.Product;
import com.chen.factorymethod.ProductA;
import com.chen.factorymethod.ProductB;
/**
 * 静态工厂 模式
 * 		去除工厂的抽象类，将产生对象的方法static
 * 		无法扩展工厂类，只能修改 ；  不符合对修改关闭，对扩展开发的原则
 * 
 * 2015-8-26 11:21:22
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) {
		
		Product productA = SimpleProductFactory.create(ProductA.class);
		productA.show();
		
		System.out.println("\n----------\n");
		
		Product productB = SimpleProductFactory.create(ProductB.class);
		productB.show();
	}
}