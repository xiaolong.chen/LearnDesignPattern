package com.chen.factorymethod;
/**
 * 负责定义产品的共性，实现对事物抽象的定义
 * 2015-8-26 10:35:19
 * @author jinlong.chen
 */
public interface Product {
	
	/**
	 * 产品名称
	 */
	public void getName();
	
	/**
	 * 产品功能
	 */
	public void getFeature();
	
	/**
	 * 展示所有属性
	 */
	public void show();
}