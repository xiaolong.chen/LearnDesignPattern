package com.chen.factorymethod;
/**
 * 工厂方法 :  
 * 		定义创建对象的接口，子类决定实例化具体类。工厂方法使类的实例化延迟到子类
 * 		优点：是简单工厂的进一步抽象和推广，使用多态性
 *      克服了简单工厂违背开放-封闭的缺点
 * 		每增加一个产品，增加产品类，适当的修改一下工厂类或扩展工厂类即可
 * 	
 * 工厂方法属于解耦框架，高层模块只需知道抽象的接口，
 * 
 * 2015-8-26 10:02:37
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) {
		
		Factory factory = new ProductsFactory();
		
		Product productA = factory.create(ProductA.class);
		productA.show();
		
		Product productB = factory.create(ProductB.class);
		productB.show();
	}
}