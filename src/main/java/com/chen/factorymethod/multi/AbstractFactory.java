package com.chen.factorymethod.multi;

import com.chen.factorymethod.Product;
/**
 * 
 * 2015-8-26 13:37:44
 * @author jinlong.chen
 */
public abstract class AbstractFactory {
	
	/**
	 * 
	 * @return
	 */
	public abstract Product create();
}