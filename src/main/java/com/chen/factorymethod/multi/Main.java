package com.chen.factorymethod.multi;

import com.chen.factorymethod.Product;
/**
 * 工厂方法 :  
 * 		定义创建对象的接口，子类决定实例化具体类。工厂方法使类的实例化延迟到子类
 * 		优点：是简单工厂的进一步抽象和推广，使用多态性
 * 		每增加一个产品，增加产品类，适当的修改一下工厂类或扩展工厂类即可
 * 
 * 产品较多，每个实现初始化的逻辑都不相同，避免产生较大的一个工厂方法，升级为多工厂，每个产品实现一个工厂类
 * 每个工厂类职责清晰，单一
 * 
 * 2015-8-26 13:17:55
 * @author jinlong.chen
 */
public class Main {
	
	
	public static void main(String[] args) {
		AbstractFactory factoryA = new ProductAFactory();
		Product productA = factoryA.create();
		productA.show();
		
		AbstractFactory factoryB = new ProductBFactory();
		Product productB =factoryB.create();
		productB.show();
	}
}