
package com.chen.factorymethod.multi;

import com.chen.factorymethod.Product;
import com.chen.factorymethod.ProductB;
/**
 * 
 * 2015-8-26 13:17:55
 * @author jinlong.chen
 */
public class ProductBFactory extends AbstractFactory {
	
	
	@Override
	public Product create() {
		/**
		 * 个性化
		 */
		return new ProductB();
	}
}