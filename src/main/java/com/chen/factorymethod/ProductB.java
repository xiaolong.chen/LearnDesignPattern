package com.chen.factorymethod;
/**
 * 具体的产品B
 * 2015-8-26 10:35:51
 * @author jinlong.chen
 */
public class ProductB implements Product {
	
	public ProductB() {
		System.out.println(" ProductB  construtor ~~");
	}
	
	@Override
	public void getName() {
		System.out.println("产品B");
	}
	
	@Override
	public void getFeature() {
		System.out.println("喝水~~");
	}
	
	@Override
	public void show() {
		System.out.println("class  :  "+this.getClass().getName()+"  ,  hashCode : "+this.hashCode());
		
		this.getName();
		this.getFeature();
	}
}