package com.chen.factorymethod;
/**
 * 具体的产品工厂类
 * 2015-8-26 10:58:22
 * @author jinlong.chen
 */
public class ProductsFactory extends Factory {
	
	public ProductsFactory() {
		System.out.println("工厂类实例化~~~  , class  :  "+this.getClass().getName()+"  ,  hashCode : "+this.hashCode());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Product> T create(Class<T> clazz) {
		Product product = null;
		try {
			/**
			 * 调用默认参数空的构造方法 , newInstance()
			 */
			product = (Product) Class.forName(clazz.getName()).newInstance();
		}
		catch(Exception e) {
			
		}
		return (T) product;
	}
}