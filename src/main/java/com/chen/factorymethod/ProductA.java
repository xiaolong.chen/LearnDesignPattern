package com.chen.factorymethod;
/**
 * 具体的产品A
 * 2015-8-26 10:35:37
 * @author jinlong.chen
 */
public class ProductA implements Product {
	
	public ProductA() {
		System.out.println(" ProductA  construtor ~~");
	}
	
	@Override
	public void getName() {
		System.out.println("产品A");
	}
	
	@Override
	public void getFeature() {
		System.out.println("盛水~~");
	}
	
	@Override
	public void show() {
		System.out.println("class  :  "+this.getClass().getName()+"  ,  hashCode : "+this.hashCode());
		
		this.getName();
		this.getFeature();
	}
}