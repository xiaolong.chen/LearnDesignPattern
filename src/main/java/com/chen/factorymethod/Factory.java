package com.chen.factorymethod;
/**
 * 抽象工厂方法
 * 2015-8-26 10:57:58
 * @author jinlong.chen
 */
public abstract class Factory {
	
	public abstract <T extends Product>T create(Class<T> clazz);
}