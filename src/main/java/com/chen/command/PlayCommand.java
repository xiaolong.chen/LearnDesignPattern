package com.chen.command;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
  * 定义一个接收者和行为之间的弱耦合；实现execute()方法，负责调用接收者的相应操作。execute()方法通常叫做执行方法
  * 2018-3-19 15:10:55
  * @author jinlong.chen
  */
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlayCommand implements Command {
	
	private AudioPlayerReceiver audioPlayerReceiver;
	
	
	@Override
	public void execute() {
		audioPlayerReceiver.play();
	}
}