package com.chen.command;
/**
 * 命令(Command)角色：声明了一个给所有具体命令类的抽象接口
  * 2018-3-19 14:51:25
  * @author jinlong.chen
  */
public interface Command {
	
	/**
	 * 命令执行
	 */
	public void execute();
}