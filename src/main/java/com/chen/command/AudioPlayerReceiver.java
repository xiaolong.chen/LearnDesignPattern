package com.chen.command;

import lombok.extern.slf4j.Slf4j;
/**
  * 接收者(Receiver)角色：负责具体实施和执行一个请求。任何一个类都可以成为接收者，实施和执行请求的方法叫做行动方法
  * 接收者角色，由播放器类扮演
  * 2018-3-19 14:48:48
  * @author jinlong.chen
  */
@Slf4j
public class AudioPlayerReceiver {
	
	public void play() {
		log.info("AudioPlayer 播放~~~");
	}
	
	public void rewind() {
		log.info("AudioPlayer 倒带~~~");
	}
	
	public void stop() {
		log.info("AudioPlayer 停止~~~");
	}
}