package com.chen.command;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
  * 请求者(Invoker)角色 : 负责调用命令对象执行请求，相关的方法叫做行动方法
  * 2018-3-19 14:56:16
  * @author jinlong.chen
  */
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KeypadInvoker {

	private Command playCommand;
	private Command rewindCommand;
	private Command stopCommand;
	
	
	public void play() {
		this.playCommand.execute();
	}
	
	public void rewind() {
		this.rewindCommand.execute();
	}
	
	public void stop() {
		this.stopCommand.execute();
	}
}