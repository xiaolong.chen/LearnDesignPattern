package com.chen.command;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
  * 
  * 2018-3-19 15:10:55
  * @author jinlong.chen
  */
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StopCommand implements Command {

	private AudioPlayerReceiver audioPlayerReceiver;
	
	
	@Override
	public void execute() {
		audioPlayerReceiver.stop();
	}
}