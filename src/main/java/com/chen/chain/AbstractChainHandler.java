package com.chen.chain;

import lombok.Setter;

/**
 * 抽象责任链处理器
 * 		责任链和请求对象通过方法传值传递，避免耦合
 * 2015-8-30 21:35:31
 * @author jinlong.chen
 */
public abstract class AbstractChainHandler {
	/**
	 * 下个处理对象
	 */
	@Setter
	protected AbstractChainHandler next;
	
	
	/**
	 * 符合规则，返回true，不再执行责任链 ; 不符合规则，返回false，继续往下执行责任链
	 * @param request 被处理对象
	 * @return
	 */
	public abstract boolean process(Request request);
	
	
	/**
	 * 执行责任链
	 */
	protected boolean doChain(Request request) {
		boolean result = this.process(request);
		System.out.println("class  :  "+this.getClass().getName()+"  ,  handleRequest result : "+result);
		
		if(next==null && !result) {
			//throws Exception
			System.out.println("no chainhander ~~~");
			return false;
		}
		
		if(next != null && !result) {
			return next.doChain(request);
		}
		
		return result;
	}
}