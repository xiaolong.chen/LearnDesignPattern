package com.chen.chain;
/**
 * 抽象处理链
 * 2015-8-30 22:19:46
 * @author jinlong.chen
 */
public class T3ChainHandler extends AbstractChainHandler {
	
	/**
	 * 处理200-300以上的
	 */
	@Override
	public boolean process(Request request) {
		if(request.getResult()<=300) {
			return true;
		}
		
		return false;
	}
}