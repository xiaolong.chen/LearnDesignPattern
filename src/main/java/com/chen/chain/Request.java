package com.chen.chain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 进入责任链被执行处理的请求
 * 		通过传值，非成员变量
 * 2015-8-30 22:00:45
 * @author jinlong.chen
 */
@Setter
@Getter
@ToString
public class Request {
	
	private int result;
	
}