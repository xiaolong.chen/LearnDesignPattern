package com.chen.chain;

import lombok.extern.slf4j.Slf4j;
/**
 * 责任链: 
 * 		使过个对象都有处理请求的机会，避免请求者和处理者的耦合关系；多个处理对象结成处理链，直到处理完为止
 * 
 * 2015-8-30 21:35:31
 * @author jinlong.chen
 */
@Slf4j
public class Main {
	
	public static void main(String[] args) {
		Request request = new Request();
		request.setResult(50);
		
		/**
		 * 建立责任链顺序关系
		 */
		AbstractChainHandler t1Handler = new T1ChainHandler();
		AbstractChainHandler t2Handler = new T2ChainHandler();
		AbstractChainHandler t3Handler = new T3ChainHandler();
		
		t1Handler.setNext(t2Handler);
		t2Handler.setNext(t3Handler);
		
		boolean flag = t1Handler.doChain(request);
		log.info("flag : {}",flag);
	}
}