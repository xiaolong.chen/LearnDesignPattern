package com.chen.chain;
/**
 * 抽象处理链
 * 2015-8-30 22:19:46
 * @author jinlong.chen
 */
public class T2ChainHandler extends AbstractChainHandler {
	
	/**
	 * 处理100-200以内的
	 */
	@Override
	public boolean process(Request request) {
		if(request.getResult()<=200) {
			return true;
		}
		
		return false;
	}
}