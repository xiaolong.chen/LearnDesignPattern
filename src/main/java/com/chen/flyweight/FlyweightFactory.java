package com.chen.flyweight;

import java.util.concurrent.ConcurrentHashMap;
/**
 * 享元工厂
 * 2016-4-21 15:49:06
 * @author jinlong.chen
 */
public class FlyweightFactory {
	
	private static final ConcurrentHashMap<String,Flyweight> INSTANCES = new ConcurrentHashMap<String,Flyweight>();
	
	
	public static Flyweight getFlyweight(String extrinsic) {
		Flyweight flyweight = INSTANCES.get(extrinsic);
		
		if(flyweight==null) {
			System.out.println("创建对象~  , extrinsic : "+extrinsic);
			INSTANCES.putIfAbsent(extrinsic,new ConcreteFlyweight(extrinsic));
			
			flyweight= INSTANCES.get(extrinsic);
		}
		
		return flyweight;
	}
}