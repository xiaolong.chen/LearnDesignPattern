package com.chen.flyweight;
/**
 * 具体享元角色
 * 2016-4-21 15:47:38
 * @author jinlong.chen
 */
public class ConcreteFlyweight extends Flyweight {
	
	public ConcreteFlyweight(String extrinsic) {
		super(extrinsic);
	}

	@Override
	public void doSth() {
		System.out.println("doSth ~~ , hashCode : "+this.hashCode());
	}
}