package com.chen.flyweight;
/**
 * 享元模式：  运用共享技术有效地支持大量细粒度对象
 * 		对象池中每个对象都是一样的，是重量级对象，解决的是复用问题
 * 		而享元是不一样的对象，且缓存的是轻量级对象，解决的是共享问题
 * 		
 * 		享元模式，大大减少了应用程序的对象，降低程序内存的占用，提供性能
 * 		提高了系统的复杂性，需要分离出外部状态和内部状态，外部状态具有固化特征，不应随内部状态改变而改变
 * 
 * 2016-4-21 14:17:49
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) {
		Flyweight a = FlyweightFactory.getFlyweight("a");
		a.doSth();
		
		Flyweight b = FlyweightFactory.getFlyweight("b");
		b.doSth();
		
		Flyweight aa = FlyweightFactory.getFlyweight("a");
		aa.doSth();
	}
}