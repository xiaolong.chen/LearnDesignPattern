package com.chen.flyweight;
/**
 * String 和 享元模式
 * 		
 * 2016-4-21 14:20:26
 * @author jinlong.chen
 */
public class TestOnString {
	
	public static void main(String[] args) {
		String a = "abc";
		final String b = "abc";
		String c = new String("abc");
		String d = "a"+"bc";
		String e = a+"";
		String f = b+"";
		
		//true
		System.out.println("a==b :"+(a==b));
		
		//a,b 都指向string pool的地址
		//变量c指向的是堆内存的地址
		//false
		System.out.println("a==c :"+(a==c));
		
		//intern() native 实现
		//a string that has the same contents as this string, but is guaranteed to be from a pool of unique strings.
		//true
		System.out.println("a==c.intern() :"+(a==c.intern()));
		
		//构建d的时，d的内容是可知的，所以 d 指向的是也 string pool地址
		//true
		System.out.println("a==d :"+(a==d));
		
		//构建e的时候，a是变量名，是不可知的，e指向的是堆内存的一个新的地址
		//false
		System.out.println("a==e :"+(a==e));
		
		//构建f的时候，b用final修饰了，是可知的，所以f指向的是string pool的地址
		//true
		System.out.println("a==f :"+(a==f));
	}
}