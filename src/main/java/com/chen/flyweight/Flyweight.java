package com.chen.flyweight;
/**
 * 抽象享元角色
 * 2016-4-21 15:48:24
 * @author jinlong.chen
 */
public abstract class Flyweight {
	//内部状态： 存储在享元对象内部，不会随着环境改变，可以共享
	private String intrinsic;
	//外部状态：是对象得以依赖的一个标记，随着环境变化，不可共享
	private final String extrinsic;
	
	public Flyweight(String extrinsic) {
		this.extrinsic = extrinsic;
	}
	
	public String getIntrinsic() {
		return intrinsic;
	}
	public void setIntrinsic(String intrinsic) {
		this.intrinsic = intrinsic;
	}
	
	public String getExtrinsic() {
		return extrinsic;
	}
	
	public abstract void doSth();
}