package com.chen.observer;
/**
 * 抽象观察者
 * 2015-8-22 10:28:59
 * @author jinlong.chen
 */
public abstract class Observer {
	
	/**
	 * 如果观察者较多，使用异步方式
	 * @param message
	 */
	public abstract void onMessage(String message);
}