package com.chen.observer;
/**
 * 具体的主题
 * 2015-8-22 11:14:53
 * @author jinlong.chen
 */
public class T1Subject extends Subject {
	//private ExecutorService executorService = null;
	
	public T1Subject() {
		/**
		 * 不保证顺序执行
		 */
		//executorService = Executors.newCachedThreadPool();
		/**
		 * 保证调度按顺序执行
		 */
		//executorService = Executors.newSingleThreadExecutor();
	}
	
	/**
	 * 顺序调用，观察者较多的时候，性能
	 * 一个扔出异常，或执行时间长，影响整体
	 */
	@Override
	public void notifyObservers(String message) {
		System.out.println("notify Observer begin , class : "+this.getClass().getName()+" , hashCode : "+this.hashCode());
		
		for(Observer observer : observers) {
			observer.onMessage(message);
		}
		
		/*for(Observer observer : observers) {
			final Observer temp = observer;
			executorService.submit(new Runnable(){
				@Override
				public void run() {
					temp.onUpdate(message);
				}
			});
		}*/
		System.out.println("notify Observer end , class : "+this.getClass().getName()+" , hashCode : "+this.hashCode());
	}
	
}