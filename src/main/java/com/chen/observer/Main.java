package com.chen.observer;
/**
 * 观察者模式：
 * 		多个对象监听一个对象的状态
 * 		被监听的对象，持有若干监听器的引用
 * 		当被监听者对象状态发生变化，监听对象实时被通知并做出响应
 * 
 * 2015-8-22 10:33:33
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) {
		Observer observer01 = new T1Observer();
		Observer observer02 = new T2Observer();
		
		System.out.println("****begin*******");
		
		Subject subject = new T1Subject();
		subject.addObserver(observer01);
		subject.addObserver(observer02);
		
		//update and notify
		subject.notifyObservers("message01");
		
		
		System.out.println("****delete one observer, update, notify************");
		subject.removeObserver(observer01);
		
		subject.notifyObservers("message02");
	}
}