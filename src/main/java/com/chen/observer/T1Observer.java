package com.chen.observer;

import java.util.Date;
/**
 * 具体的观察者1
 * 2015-8-22 11:15:01
 * @author jinlong.chen
 */
public class T1Observer extends Observer {
	
	
	@Override
	public void onMessage(String message) {
		try {
			Thread.sleep(1500);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(this.getClass().getName()+"   ,   "+
				message.toString()+"  , hashCode : "+
				this.hashCode()+" , "+
				new Date());
	}
}