package com.chen.observer;

import java.util.Date;
/**
 * 具体的观察者2
 * 2015-8-22 11:15:20
 * @author jinlong.chen
 */
public class T2Observer extends Observer {
	
	
	@Override
	public void onMessage(String message) {
		try {
			Thread.sleep(500);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(this.getClass().getName()+"   ,   "+
				message.toString()+"  , hashCode : "+
				this.hashCode()+" , "+
				new Date());
	}
}