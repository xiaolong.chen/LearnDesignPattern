package com.chen.observer;

import java.util.LinkedList;
import java.util.List;
/**
 * 抽象主题， 被观察者
 * 2015-8-22 10:28:47
 * @author jinlong.chen
 */
public abstract class Subject {
	protected List<Observer> observers = new LinkedList<Observer>();
	
	public void addObserver(Observer observer) {
		observers.add(observer);
	}
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}
	
	/**
	 * 通知
	 */
	public abstract void notifyObservers(String message);
}