package com.chen.builder;

import java.util.ArrayList;
import java.util.List;
/**
 * 构建者模式：
 * 		将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同表示
 * 
 * 构建者模式使客户端不必知道产品内部组成的细节
 * 构建者独立，容易扩展
 * 便于控制细节风险
 * 
 * 		使用场景
 * 			1. 相同的方法，不同的执行顺序，产生不同的事件结果
 * 			2. 多个部件或零件，都可以装配到一个对象中，产生的运行结果不相同
 * 
 * 2016-4-20 18:07:18
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) {
		
		List<String> sequences = new ArrayList<String>();
		sequences.add("alarm");
		sequences.add("start");
		sequences.add("engineBoom");
		sequences.add("stop");
		sequences.add("restart");
		
		CarBuilder carBuilder = new BenzBuilder();
		carBuilder.setSequence(sequences);
		
		CarModel carModel = carBuilder.build();
		carModel.run();
	}
}