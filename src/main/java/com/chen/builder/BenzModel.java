package com.chen.builder;
/**
 * 具体产品类
 * 2016-4-21 10:18:47
 * @author jinlong.chen
 */
public class BenzModel extends CarModel {
	
	@Override
	protected void start() {
		System.out.println("benz car start,start~~");
	}
	
	@Override
	protected void alarm() {
		System.out.println("benz car alarm, alarm~~");
	}
	
	@Override
	protected void engineBoom() {
		System.out.println("benz car engineBoom, engineBoom~~");
	}
	
	@Override
	protected void stop() {
		System.out.println("benz car stop, stop~~");
	}
}