package com.chen.builder;
/**
 * 具体构建者
 * 2016-4-21 11:13:01
 * @author jinlong.chen
 */
public class BenzBuilder extends CarBuilder {
	
	@Override
	public CarModel build() {
		CarModel carModel = new BenzModel();
		carModel.setSequence(super.sequences);
		
		return carModel;
	}
}