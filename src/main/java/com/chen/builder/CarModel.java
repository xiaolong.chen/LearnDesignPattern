package com.chen.builder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * 
 * 2016-4-21 10:18:47
 * @author jinlong.chen
 */
public abstract class CarModel {
	private List<String> sequences = new ArrayList<String>();
	
	public void setSequence(List<String> sequences) {
		this.sequences = sequences;
	}
	
	protected abstract void start();
	protected abstract void alarm();
	protected abstract void engineBoom();
	protected abstract void stop();
	
	public final void run() {
		System.out.println("@@begin run~~");
		
		Iterator<String> iterator = sequences.iterator();
		while(iterator.hasNext()) {
			String sequence = iterator.next();
			if(sequence.equals("start")) {
				this.start();
			}
			else if(sequence.equals("alarm")) {
				this.alarm();
			}
			else if(sequence.equals("engineBoom")) {
				this.engineBoom();
			}
			else if(sequence.equals("stop")) {
				this.stop();
			}
			else {
				System.out.println("sequence element  error !!!!!!!!");
			}
		}
		
		System.out.println("@@after run~~");
	}
}