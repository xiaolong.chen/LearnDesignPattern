package com.chen.builder;

import java.util.ArrayList;
import java.util.List;
/**
 * 抽象构建者
 * 2016-4-21 11:13:01
 * @author jinlong.chen
 */
public abstract class CarBuilder {
	protected List<String> sequences = new ArrayList<String>();
	
	public void setSequence(List<String> sequences) {
		this.sequences = sequences;
	}
	
	public abstract CarModel build();
}