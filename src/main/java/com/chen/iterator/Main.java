package com.chen.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
/**
 * 迭代器模式
 * 		定义一套遍历接口
 * 		由各集合容器实现(多为内部类)，隐藏底部存储细节, 由一套标准的接口遍历集合
 * 
 * 2016-4-20 17:35:44
 * @author jinlong.chen
 */
@Slf4j
public class Main {
	
	public static void main(String[] args) {
		
		List<String> strs = new ArrayList<String>();
		strs.add("QQQ");
		strs.add("AAA");
		strs.add("EEE");
		strs.add("III");
		strs.add("PPP");
		
		int count = 0;
		Iterator<String> iterator = strs.iterator();
		while(iterator.hasNext()) {
			count++;
			
			String str = iterator.next();
			log.info("count :{}, str:{}",count,str);
		}
	}
}