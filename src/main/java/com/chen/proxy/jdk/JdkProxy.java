package com.chen.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
/**
 * jdk InvocationHandler 接口实现代理， 委托类必要实现接口
 * 2015-8-27 14:54:04
 * @author jinlong.chen
 */
public class JdkProxy implements InvocationHandler {
	private Object object = null;
	
	public JdkProxy() {
		
	}
	
	public Object bind(Object object) {
        this.object = object;
        
        return Proxy.newProxyInstance(object.getClass().getClassLoader(),object.getClass().getInterfaces(),this);
    }
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		long begin = System.currentTimeMillis();
		long end = 0;
		
		Thread.sleep(100);
		/**
		 * doSth
		 */
		Object returnValue = method.invoke(this.object,args);
		
		end = System.currentTimeMillis();
		
		System.out.println("委托类："+proxy.getClass().getName()+"  ,  方法："+method.getName());
		System.out.println("代理耗时："+(end-begin)+" ms  , Proxy class : "+this.getClass().getName()+"  , hashCode : "+this.hashCode());
		return returnValue;
	}
}