package com.chen.proxy;
/**
 * 委托类接口， 使用jdk做代理的时候，必须实现接口
 * 2014-10-29 0:15:15
 * @author chenjl
 */
public interface Say {
	
	public void helloWorld();
	
	public int sayHello(String name);
}