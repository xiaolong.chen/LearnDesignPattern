package com.chen.proxy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * delegate 静态代理
 * 2016-3-14 16:30:34
 * @author jinlong.chen
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SayDelegate implements Say {
	
	private Say say;
	
	
	@Override
	public void helloWorld() {
		this.say.helloWorld();
	}
	
	@Override
	public int sayHello(String name) {
		return this.say.sayHello("chenjl");
	}
}