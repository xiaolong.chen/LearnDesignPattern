package com.chen.proxy;

import org.junit.Test;

import com.chen.proxy.cglib.CglibProxy;
import com.chen.proxy.jdk.JdkProxy;
/**
 * 代理模式
 * 		为其他对象提供一种代理以控制这个对象的访问
 * 		切面 aspect ， 切入点 joinpiont， 通知 advice ， 织入 weave
 * 
 * cglib 创建动态代理对象的性能比jdk创建代理对象性能高，花费的时间比jdk创建代理时间长
 * 单例或者对象池的代理，适合使用cglib
 * cglib 采用动态创建子类的方式生成代理对象，不能对目标类final方法代理
 * 
 * 2015-8-27 14:34:45
 * @author jinlong.chen
 */
public class Main {
	public static void main(String[] args) {
		
		System.out.println(" 原生： ");
		
		Say say01 = new SayImpl();
		say01.helloWorld();
		System.out.println("sayHello 返回值 ："+say01.sayHello("chenjl")+"  , hashCode : "+say01.hashCode());
		
		
		
		System.out.println("\n jdk代理：");
		/**
		 * 使用jdk的代理
		 */
		JdkProxy jdkProxy = new JdkProxy();
		Say say02 = (Say) jdkProxy.bind(new SayImpl());
		say02.helloWorld();
		System.out.println("sayHello 返回值 ："+say02.sayHello("chenjl")+"  , hashCode : "+say02.hashCode());
		
		
		
		System.out.println("\n cglib代理：");
		/**
		 * 使用cglib的代理
		 */
		CglibProxy cglibProxy = new CglibProxy();
		SayImpl say03 = (SayImpl) cglibProxy.createPoxyInstrance(new SayImpl());
		say03.helloWorld();
		System.out.println("sayHello 返回值 ："+say03.sayHello("chenjl")+"  , hashCode : "+say03.hashCode());
	}
	
	@Test
	public void testOnDelegate() {
		Say say = new SayImpl();
		
		SayDelegate sayDelegate = new SayDelegate();
		sayDelegate.setSay(say);
		
		sayDelegate.helloWorld();
		System.out.println(sayDelegate.sayHello("chenjl"));
	}
}