package com.chen.proxy;
/**
 * 委托类实现
 * 2014-10-29 0:16:26
 * @author chenjl
 */
public class SayImpl implements Say {
	
	
	@Override
	public void helloWorld() {
		System.out.println("hello world !    class : "+this.getClass().getName()+" , hashCode : "+this.hashCode());
	}
	
	
	@Override
	public int sayHello(String name) {
		System.out.println("hello "+name+" !   class : "+this.getClass().getName()+" , hashCode : "+this.hashCode());
		
		return 10;
	}
}