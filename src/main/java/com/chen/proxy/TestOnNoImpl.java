package com.chen.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
/**
 * 
 * 2016-8-26 13:05:37
 * @author chenjinlong
 */
public class TestOnNoImpl {
	
	public static void main(String[] args) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		
		Say say= (Say) Proxy.newProxyInstance(classLoader,new Class[]{Say.class},new InvocationHandler(){
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				String methodName = method.getName();
				Class<?> returnType = method.getReturnType();
				System.out.println("委托类："+proxy.getClass().getName()+"  ,  方法："+methodName+" , returnType : "+returnType.getName());
				
				if(args!=null) {
					Class<?>[] paramTypes = method.getParameterTypes();
					System.out.println("methodName : "+methodName+" , 参数个数："+args.length);
					
					for(Class<?> paramType : paramTypes) {
						System.out.println("methodName : "+methodName+" , 参数类型："+paramType.getName());
					}
				}
				
				//do by remote invoker
				
				return 1;
			}
		});
		
		say.helloWorld();
		say.sayHello("chenjl");
	}
}