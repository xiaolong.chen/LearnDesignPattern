package com.chen.templatemethod;
/**
 * 模板方法模式 : 
 * 	定义一个操作的算法骨干，将这些步骤延迟到子类中 ,使得子类可以不改变算法结构可重新定义算法的某些特定步骤
 * 
 * 		1. 使用abstract方法，在子类中实现[多态]，父类调用子类的实现方法
 * 		2. 将不变行为搬移到父类，去除子类的重复代码，提供了很好的代码复用平台
 * 
 * 2016-1-22 11:34:09
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) {
		HummerModel hummerModel01 = new HummerH1Model();
		hummerModel01.run();
		
		
		System.out.println("\n");
		
		
		HummerModel hummerModel02 = new HummerH1Model() {
			@Override
			protected void start() {
				super.start();
				System.out.println("###HummerH1Model start");
			}
			
			@Override
			protected boolean isAlarm() {
				System.out.println("##isAlarm judge");
				return false;
			}
		};
		hummerModel02.run();
	}
}