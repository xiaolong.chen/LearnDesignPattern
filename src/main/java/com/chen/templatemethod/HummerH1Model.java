package com.chen.templatemethod;
/**
 * 具体实现模板类
 * 2016-1-22 11:49:40
 * @author jinlong.chen
 */
public class HummerH1Model extends HummerModel {
	
	
	/**
	 * 算法的某些特定步骤在子类中实现
	 */
	@Override
	protected void start() {
		System.out.println("    #HummerH1Model start");
	}
	
	@Override
	protected void alarm() {
		System.out.println("    #HummerH1Model alarm ");
	}
	
	@Override
	protected void engineBoom() {
		System.out.println("    #HummerH1Model engineBoom");
	}
	
	@Override
	protected void stop() {
		System.out.println("    #HummerH1Model stop stop");
	}
}