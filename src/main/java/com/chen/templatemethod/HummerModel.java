package com.chen.templatemethod;
/**
 * 抽象模板类, 对外只暴漏 流程run方法， 其他方法为 protected
 * 2016-1-22 11:43:44
 * @author jinlong.chen
 */
public abstract class HummerModel {
	/**
	 * 抽象方法，留在子类中实现
	 * 算法的某些特定步骤
	 */
	protected abstract void start();
	protected abstract void alarm();
	protected abstract void engineBoom();
	protected abstract void stop();
	
	/**
	 * 默认实现方法，子类可重写
	 */
	protected boolean isAlarm() {
		System.out.println("@@isAlarm judge");
		return true;
	}
	
	/**
	 * 抽象的算法骨干
	 * final 不允许被重写
	 */
	public final void run() {
		System.out.println("@@begin run~~");
		
		this.start();
		if(this.isAlarm()) {
			this.alarm();
		}
		this.engineBoom();
		this.stop();
		
		System.out.println("@@after run~~");
	}
}