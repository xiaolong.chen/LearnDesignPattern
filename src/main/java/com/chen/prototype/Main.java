package com.chen.prototype;
/**
 * 原型模式
 * 		用原型实例指定创建对象的种类，通过拷贝原型创建新的对象， 避免创建的细节
 * 		内存二进制流的拷贝，性能良好，避免构造函数的约束
 * 		陷阱： 浅拷贝 ， 避免成员字段为final
 * 
 * 		clone产生对象不调用构造函数 ;  使用反射创建对象=调用构造函数
 * 
 * 2015-8-28 21:33:25
 * @author jinlong.chen
 */
public class Main {
	
	public static void main(String[] args) throws CloneNotSupportedException {
		
		/**
		 * 测试浅拷贝
		 */
		System.out.println("    ~浅度拷贝~");
		int num1 = 1001;
		String address1 = "shanghai";
		TObject tObject1 = new TObject("object11");
		
		Prototype1 prototype11 = new Prototype1();
		prototype11.setNum(num1);
		prototype11.setAddress(address1);
		prototype11.setTObject(tObject1);
		
		System.out.println(prototype11);
		
		
		Prototype1 prototype12 = (Prototype1) prototype11.clone();
		
		num1 = 1002;
		address1 = "beijing";
		tObject1.setName("object12");;
		
		System.out.println(prototype11);
		System.out.println(prototype12);
		
		
		/**
		 * 深度拷贝
		 */
		System.out.println("    ~深度拷贝~");
		
		int num2 = 1003;
		String address2 = "zhengzhou";
		TObject tObject2 = new TObject("object21");
		
		Prototype2 prototype21 = new Prototype2();
		prototype21.setNum(num2);
		prototype21.setAddress(address2);
		prototype21.setTObject(tObject2);
		
		System.out.println(prototype21);
		
		
		Prototype2 prototype22 = (Prototype2) prototype21.clone();
		
		num2 = 1004;
		address2 = "wuhan";
		tObject2.setName("object22");
		
		System.out.println(prototype21);
		System.out.println(prototype22);
	}
}