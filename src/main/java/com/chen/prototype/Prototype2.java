package com.chen.prototype;
/**
 * 原型模式 ： 深拷贝
 * 		修改clone，Object和数组的引用使用产生新对象，避免浅拷贝
 * 
 * 2015-8-28 21:32:42
 * @author jinlong.chen
 */
public class Prototype2 extends Prototype1{
	
	
	/**
	 * 重写clone方法
	 * 深度clone
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Prototype2 object = (Prototype2) super.clone();
		object.tObject = new TObject(object.getTObject().getName());
		
		return object;
	}
}