package com.chen.prototype;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * 测试原型模式使用对象
 * 2015-8-28 21:49:02
 * @author jinlong.chen
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TObject {
	
	private String name;
}