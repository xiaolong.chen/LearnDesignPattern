package com.chen.prototype;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
/**
 * 原型模式 ： 浅拷贝
 * 		int和String 以外类型的字段，拷贝引用
 * 		String由jvm string pool做特殊处理
 * 
 * 2015-8-28 21:32:42
 * @author jinlong.chen
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Prototype1 implements Cloneable{
	
	protected int num;
	protected String address;
	protected TObject tObject;

	
	/**
	 * clone 默认为protected方法，需要修改为public
	 * 使用close必须实现Cloneable接口
	 * 
	 *  默认为浅度拷贝
	 *  	int 原生类型和String 值拷贝
	 *     集合类和自定义对象不拷贝；拷贝后的对象，属性字段在内存中仍然指向原来的对象
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}